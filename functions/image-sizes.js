/*
 * Returns:
 * [
 *  {
 *    prefix: '1005x1260',
 *    width: 1005,
 *    height: 1260
 *  },
 *  {
 *    prefix: '1005x666',
 *    width: 1005,
 *    height: 666
 *  },
 *  {
 *    prefix: '940x360',
 *    width: 940,
 *    height: 360
 *  },
 *  {
 *    prefix: '880x309',
 *    width: 880,
 *    height: 309
 *  },
 *  {
 *    prefix: '581x459',
 *    width: 581,
 *    height: 459
 *  },
 *  {
 *    prefix: '375x180',
 *    width: 375,
 *    height: 180
 *  },
 *  {
 *    prefix: '324x324',
 *    width: 324,
 *    height: 324
 *  },
 *  {
 *    prefix: '68x68',
 *    width: 68,
 *    height: 68
 *  }
 * ]
 */

const imageSizes = [
  // [width, height[
  [1340,720],
  [1005,1260],
  [1005,666],
  [940,360],
  [880,309],
  [581,459],
  [375,180],
  [324,324],
  [68,68],
];

let IMAGE_SIZES = [
  ...imageSizes.map( imageSize => {
    const width = imageSize[0]
    const height = imageSize[1]
    return {
      prefix: `${width}x${height}`,
      width,
      height,
    }
  })
];

module.exports = IMAGE_SIZES;
