// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions')
const cors = require('cors')({origin: true});

// Thumbs stuff
const ogs = require('open-graph-scraper');
const { Storage } = require('@google-cloud/storage');
const sharp = require('sharp')
const path = require('path');
const os = require('os');
const IMAGE_SIZES = require('./image-sizes');

// Elasticsearch imports
const { Client } = require('@elastic/elasticsearch')
const ElasticsearchOptions = {
  node: 'https://search-elastic-onda-mx-wtsuitsnilxmngwmodxxmzysci.us-east-2.es.amazonaws.com'
}

// Mainchimp
var Mailchimp = require('mailchimp-api-v3');
var mailchimp = new Mailchimp('787d2b80e75bc9d13c083b644c50bbf5-us17');
var mailchimpListId = '1a95a45161';

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin')
admin.initializeApp()

// Create User
exports.createUser = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    const tokenId = request.get('Authorization');

    // Verify token
    admin.auth().verifyIdToken(tokenId).then( decodedToken => {
      const { email, displayName, password } = request.query

      return admin.auth().createUser({
        email,
        password,
        displayName,
      })
    }).then(userRecord => {

      return response.status(200).send(userRecord.uid);

    }).catch(error => {

      return response.status(400).send(error);

    });
  });
});

// Update User
exports.updateUser = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    const tokenId = request.get('Authorization');

    // Verify token
    admin.auth().verifyIdToken(tokenId)
      .then( decodedToken => {
        const { uid, email, password, displayName } = request.query

        return admin.auth().updateUser(uid, {
          email,
          password,
          displayName,
        })

      }).then(userRecord => {

        return response.status(200).send(userRecord.uid);

      }).catch(error => {

        return response.status(400).send(error);

      });
  });
});

// Delete User
exports.deleteUser = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    const tokenId = request.get('Authorization');

    // Verify token
    admin.auth().verifyIdToken(tokenId)
      .then( decodedToken => {
        const { uid } = request.query;

        return admin.auth().deleteUser(uid)

      }).then(() => {

        return response.status(200).send('success');

      }).catch(error => {

        return response.status(400).send(error);

      });
  });
});

// Scrape URL
exports.scrapeUrl = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    const options = {'url': request.query.targetUrl};
    ogs(options)
      .then((result) => {
        return response.status(200).send(result);
      })
      .catch((error) => {
        return response.status(400).send(error);
      });
  });
});

// Image Sizing
exports.generateThumbnail = functions.storage.object('media/{imageId}').onFinalize((object, context) => {
  const fileBucket = object.bucket; // The Storage bucket that contains the file.
  const filePath = object.name; // File path in the bucket.
  const contentType = object.contentType; // File content type.
  const resourceState = object.resourceState; // The resourceState is 'exists' or 'not_exists' (for file/folder deletions).
  const metageneration = object.metageneration; // Number of times metadata has been generated. New objects have a value of 1.

  // Check mime type
  if (!contentType.startsWith('image/')) {
    return false;
  }

  // Check if this is an already proccesed image
  if (filePath.includes('_thumb')) {
    return false;
  }

  /*
   * Example. if filePath is `media/pikachu_300_thumb.jpg`
   *
   * fileName: `pikachu.jpg`
   * extension: `jpg`
   * name: `pikachu`
   *
   */
  const fileName = filePath.split('/').pop();
  const extension = fileName.split('.').pop();
  const name = fileName.replace(/\.[^/.]+$/, "");

  const storage = new Storage();
  const bucket = storage.bucket(fileBucket);

  const file = bucket.file(filePath);
  const tempFilePath = path.join(os.tmpdir(), fileName);

  return file
    .download({
      destination: tempFilePath
    })
    .then(() => {
      // Array used to store promises
      let imagePromises = [];

      // We iterate sizes
      IMAGE_SIZES.forEach( size => {
        const { prefix, width, height } = size;

        let newFileName = `${name}_${prefix}_thumb.${extension}`
        let newFileTemp = path.join(os.tmpdir(), newFileName);
        let newFilePath = `media/${newFileName}`

        // We push promises to the array
        imagePromises.push( sharp(tempFilePath)
          .resize(width, height)
          .jpeg({
            quality: 70,
            progressive: true,
          })
          .toFile(newFileTemp)
          .then( () => {
            // Upload to bucket
            return bucket.upload(newFileTemp, {
              destination: newFilePath
            });
          })
          .then( res => {
            return true;
          })
          .catch(error => console.log(error))
        );

      });

      // Return all promises. This means that the next `then()` wont't execute
      // until all promises are resolved
      return Promise.all(imagePromises);
    })
    .catch(error => console.log(error));
});

// -- Remove thumbnails when an image is deleted from the database
exports.deleteThumbnails = functions.firestore.document('media/{mediaId}').onDelete((snap, context) => {
  // Get an object representing the document
  const doc = snap.data();

  // Get the name of the file
  const name = doc.metadata.name
  // Get the name of the bucket where it was stored
  const bucketName = doc.metadata.bucket
  // Get the filename without extension
  const filename = name.split('.')[0]

  // Init new bucket instance
  const storage = new Storage();
  const bucket = storage.bucket(bucketName);


  // Delete all files that have the filename as prefix, aka thumbnails
  return bucket.deleteFiles({
    prefix: `media/${filename}`
  })
    .then( res => {
      return true
    })
    .catch(error => console.log(error))

})

// ---- ELASTICSEARCH
const prepareDoc = (doc) => {
  if(!doc.localizedContent) {
    return doc
  }

  let localizedContent = doc.localizedContent

  console.log('ORIGINAL',doc.localizedContent)

  Object.keys(doc.localizedContent).forEach( key => {
    localizedContent = Object.assign({}, localizedContent, {
      [key]: Object.assign({}, localizedContent[key], {
        // The following fields are the ones we care
        mainContent: JSON.parse(localizedContent[key].mainContent).blocks.map( item => item.text).join(' ')
      })
    })
  })

  return Object.assign({}, doc, {
    localizedContent
  })
}

const createOnElasticSearch = (index, snap, context) => {
  // Get an object representing the document
  const doc = snap.data();
  const docId = context.params.docId

  const elasticsearchClient = new Client(ElasticsearchOptions)

  const readyDoc = prepareDoc(doc)

  return elasticsearchClient.create({
    id: docId,
    index: index,
    body: readyDoc,
    type: '_doc',
  })
    .then(response => {
      console.log(`${index}/${docId} created in Elasticsearch`)
      return
    })
    .catch(console.err)
}

const updateOnElasticsearch = (index, change, context) => {
  // Get an object representing the document
  const newData = change.after.data();
  const docId = context.params.docId

  const elasticsearchClient = new Client(ElasticsearchOptions)
  // elasticsearchClient.info(console.log)

  const readyDoc = prepareDoc(newData)

  return elasticsearchClient.update({
    id: docId,
    index: index,
    type: '_doc',
    body: {
      doc: readyDoc,
      doc_as_upsert: true,
    }
  })
    .then(response => {
      console.log(`${index}/${docId} updated in Elasticsearch`)
      return
    })
    .catch(console.err)
}

const deleteOnElasticsearch = (index, snap, context) => {
  const docId = context.params.docId

  const elasticsearchClient = new Client(ElasticsearchOptions)

  return elasticsearchClient.delete({
    id: docId,
    index: index,
    type: '_doc',
  })
    .then(response => {
      console.log(`${index}/${docId} deleted in Elasticsearch`)
      return
    })
    .catch(console.err)
}

// SPACES
exports.createSpaceOnElasticsearch = functions.firestore.document('spaces/{docId}').onCreate((snap, context) =>  createOnElasticSearch('spaces', snap, context))
exports.updateSpaceOnElasticsearch = functions.firestore.document('spaces/{docId}').onUpdate((change, context) =>  updateOnElasticsearch('spaces', change, context))
exports.deleteSpaceOnElasticsearch = functions.firestore.document('spaces/{docId}').onDelete((snap, context) =>  deleteOnElasticsearch('spaces', snap, context))

// EVENTS
exports.createEventOnElasticsearch = functions.firestore.document('events/{docId}').onCreate((snap, context) =>  createOnElasticSearch('events', snap, context))
exports.updateEventOnElasticsearch = functions.firestore.document('events/{docId}').onUpdate((change, context) =>  updateOnElasticsearch('events', change, context))
exports.deleteEventOnElasticsearch = functions.firestore.document('events/{docId}').onDelete((snap, context) =>  deleteOnElasticsearch('events', snap, context))

// ARTICLES
exports.createArticleOnElasticsearch = functions.firestore.document('articles/{docId}').onCreate((snap, context) =>  createOnElasticSearch('articles', snap, context))
exports.updateArticleOnElasticsearch = functions.firestore.document('articles/{docId}').onUpdate((change, context) =>  updateOnElasticsearch('articles', change, context))
exports.deleteArticleOnElasticsearch = functions.firestore.document('articles/{docId}').onDelete((snap, context) =>  deleteOnElasticsearch('articles', snap, context))

// MAILCHIMP
const subscribeMailchimpUser = (snap, context) => {
  const doc = snap.data();

  mailchimp.post(`/lists/${mailchimpListId}/members`, {
    email_address: doc.email,
    status: 'subscribed',
    merge_fields: {
      FNAME: doc.firstName,
      LNAME: doc.lastName,
    }
  })
    //.then(results => {
      //console.log(results)
      //return true
    //})
    .catch(console.err)
}

exports.subscribeMailchimpUser = functions.firestore.document('users/{docId}').onCreate((snap, context) =>  subscribeMailchimpUser(snap, context))
