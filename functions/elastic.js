// Elasticsearch imports
const { Client } = require('@elastic/elasticsearch')
const elasticsearchClient = new Client({
  node: 'https://search-elastic-onda-mx-wtsuitsnilxmngwmodxxmzysci.us-east-2.es.amazonaws.com'
  //cloud: {
    //id: 'onda-dev:dXMtZWFzdC0xLmF3cy5mb3VuZC5pbyRhOWYwMTNkOWQ4NGQ0ZWZiYWU5MjAzMTRkMDZkZjI4NyQ4ZmI5YjY0NDYxN2U0MDZmYmVkMDY3MmI4NThiNjkzNg==',
    //username: 'elastic',
    //password: 'sddLCHLgIRm3ANVCJBbfRBIe'
  //}
})

elasticsearchClient.info(console.log)


// --- CREATE

//elasticsearchClient.update({
  //id: 'test',
  //index: 'test',
  //type: '_doc',
  //body : {
    //en:
    //{ featuredSummary: '',
      //mainContent: '{"blocks":[{"key":"3l1mm","text":"OMR presents ICE NEWS & FREEWAY FETISHES, a selection of new works by French artist Yann Gerstberger. This marks Gerstberger’s first solo exhibition in OMR, representing his multi-faceted practice while combining a variety of imagery and sources from Surrealism to Nigerian folk art, graffiti to digital image. The works include tapestries made of household materials, sculptural figures, and a site-specific, chalk mural conceived specifically for the occasion. The chalk mural will act as a backdrop for the tapestries, incorporating them into the Brutalist architecture of the gallery space, while the sculptures hold court in the otherworldly environment. ","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":13,"length":27,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"fkfpu","text":"OMR","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}',
      //name: 'Yann Gertsberger',
      //seoDescription: 'OMR presents ICE NEWS & FREEWAY FETISHES, a selection of new works by French artist Yann Gerstberger. This marks Gerstberger’s first solo exhibition in OMR.',
      //seoTitle: 'Yann Gerstberger' },
    //es:
    //{ featuredSummary: '',
      //mainContent: '{"blocks":[{"key":"3l1mm","text":"OMR presenta ICE NEWS & FREEWAY FETISHES de Yann Gerstberger. Esta muestra marca la primera exhibición individual de Gerstberger en OMR; en ella se representa su práctica multifacética, combinando una variedad de imágenes y recursos que comprenden del Surrealismo hasta el Arte Popular Nigeriano, del graffiti hasta la imagen digital. Las obras incluyen tapices hechos de materiales para el hogar, piezas escultóricas, así como un mural de tiza, creado específicamente para la ocasión. El mural de gis funcionará como escenario para los tapices, incorporándolos en la arquitectura brutalista propia del espacio de la galería; mientras las esculturas se harán presentes en el ambiente sobrenatural.","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":13,"length":28,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"18joi","text":"OMR","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}',
      //name: 'Yann Gertsberger',
      //seoDescription: 'OMR presenta ICE NEWS & FREEWAY FETISHES de Yann Gerstberger. Esta muestra marca la primera exhibición individual de Gerstberger en OMR.',
      //seoTitle: 'Yann Gerstberger' } }
//})
  //.then(response => {
    //console.log('response', response)
    //return
  //})
  //.catch(console.err)

// --- GET

elasticsearchClient.get({
  id: 'test',
  index: 'test',
  type: '_doc',
})
  .then(response => {
    console.log('response', response)
    return
  })
  .catch(console.err)

// --- DELETE

elasticsearchClient.delete({
  id: 'test',
  index: 'test',
  type: '_doc',
})
  .then(response => {
    console.log('response', response)
    return
  })
  .catch(console.err)

